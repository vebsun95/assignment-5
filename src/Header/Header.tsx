import { useContext } from "react";
import { UserContext } from "../Context/UserContext";
import "./Header.css"
import UserIcon from "./UserIcon";
import Logo from '../lost-in-translation-logo.svg';

//The top portion of every webpage, should let you navigate to the user profile
function Header() {
    const [user,] = useContext(UserContext);
    return (
    <div className="Header">
        <img className="Logo" src="/lost-in-translation-logo.svg"/>
        <h1>Lost in Translation</h1>
        {user && <UserIcon username={user.username} />}
    </div>)
}

export default Header;