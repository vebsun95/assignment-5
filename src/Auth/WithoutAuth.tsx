import { useContext } from "react";
import { Navigate } from "react-router-dom";
import { UserContext } from "../Context/UserContext";

//Used to redirect to translation page if user is already stored in context. See UserContext for how user is store.
//This enables silent login
const WithoutAuth  = (Component: React.FC) => (props: JSX.IntrinsicAttributes) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [user, ] = useContext(UserContext);
    if(user === null)
        return <Component {...props} />
    return <Navigate to="/translation" />
} 

export default WithoutAuth;
