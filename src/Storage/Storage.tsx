interface StorageKeyValue {
  "translate-user" : number,
}

// Saves the given key-value pair to the users local storage.
export const storageSave = <K extends keyof StorageKeyValue>(key : K, value : StorageKeyValue[K]) => {
  localStorage.setItem(key, JSON.stringify(value))
}

// Reades the users local storage and return the value corresponding to the given key.
export const storageRead = <K extends keyof StorageKeyValue>(key : K): StorageKeyValue[K] | null => {
  const data = localStorage.getItem(key);
  if(data){
    return JSON.parse(data)
  }

  return null;
}

// Deletes the content at the given key in the users local storage.
export const storageDelete = <K extends keyof StorageKeyValue>(key : K) => {
  localStorage.removeItem(key);
}
