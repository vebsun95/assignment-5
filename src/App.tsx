import './App.css';
import Header from './Header/Header';
import { Outlet } from 'react-router-dom';

function App() {
  /* 
   * Outlet returns the component the dom-router decieds to render.
  */
  return (
    <>
      <Header />
      <div className='Route'>
        <Outlet />
      </div>
    </>
  );
}

export default App;
