import { User } from "../Context/UserContext";

//Creates a user with the given name
//param name
//returns [message,data], message will contain any error message, data the retrieved user
async function createUser(username: string): Promise<[null, User] | [string, null]>{
  try{
  const response = await fetch(`https://ove-noroff-api.herokuapp.com/translations/`, {
      method: 'POST',
      headers: {
        'X-API-KEY': ''+process.env.REACT_APP_API_KEY,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        translations: [],
      })
    })
    if(!response.ok){
      throw new Error('Could not complete request')
    }
    const jsonResponse = await response.json() as User;
    return [null, jsonResponse]
  }catch(error){
    let message = 'Unkown error'
    if(error instanceof Error) message = error.message;
    return [message, null]
  }
}

export default createUser;