import { User } from "../Context/UserContext";

//Returns a user that has the 
//Param UserId
//returns [message,data], message will contain any error message, data the retrieved user
const getUserById = async (id : number): Promise<[string, null] | [null, User]> => {
  try{
    const response = await fetch(`https://ove-noroff-api.herokuapp.com/translations?id=${id}`)
    if(!response.ok){
      throw new Error('Could not complete request')
    }
    const data = await response.json();
    if(data.length > 0)
      return [null, data[0]];
    else{
      throw Error("User not found");
    }
  }
  catch(error){
    let message = 'Unkown error'
    if(error instanceof Error) message = error.message;
    return [message, null]
  }
}

export default getUserById;